import dynamicSort from './dynamicSort.js'

export function task_2(par) {
    var {svg, margin, width, height, g, x0, x1, y, z, data} = par;

    var meth_qual_chg_buf = data.map( d => {
        return {
            quality: d.quality,
            method: d.method,
            change: d.change,
            bufSize: d.bufSize,
            qoe: d.qoe
        }
    })
    
    console.log(meth_qual_chg_buf)

    var grouped_data = d3.nest()
        .key(function(d) { return d.bufSize;})
        .key(function(d) { return d.method;})
        .rollup(function(d) { 
            return d3.mean(d, function(g) {return g.qoe; });
        })
        .entries(meth_qual_chg_buf)
        .map( group => {
            var obj = {}
            obj.bufSize = group.key
            var key_ = group.values.map(g => g.key)
            // console.log(key_)
            var val_ = group.values.map(g => g.value)
            // console.log(val_)
            for (var i=0; i < key_.length; ++i)
                obj[key_[i]] = val_[i]
            // console.log(obj)
            return obj;
        })

    console.log(grouped_data)
    grouped_data.sort(dynamicSort('bufSize'))
    grouped_data.push(grouped_data[2])
    grouped_data[2] = grouped_data[1]
    grouped_data[1] = grouped_data[0]
    grouped_data[0] = grouped_data[3]
    grouped_data.pop()
    console.log(grouped_data)

    var keys = Object.keys(grouped_data[0]).slice(1)
    console.log(keys)


    x0.domain(grouped_data.map(function(d) { return d.bufSize; }));
    x1.domain(keys).rangeRound([0, x0.bandwidth()]);
    y.domain([0, d3.max(grouped_data, function(d) { return d3.max(keys, function(key) { return d[key]; }); })]).nice();

    g.append("g")
        .selectAll("g")
        .data(grouped_data)
        .enter().append("g")
        .attr("transform", function(d) { return "translate(" + x0(d.bufSize) + ",0)"; })
        .selectAll("rect")
        .data(function(d) { return keys.map(function(key) { return {key: key, value: d[key]}; }); })
        .enter().append("rect")
        .attr("x", function(d) { return x1(d.key); })
        .attr("y", function(d) { return y(d.value); })
        .attr("width", x1.bandwidth())
        .attr("height", function(d) { return height - y(d.value); })
        .attr("fill", function(d, i) { return z(i) });

    g.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x0))
        .append("text")
            .attr("x", width-50)
            .attr("y", 30)
            .attr("fill", "#000")
            .attr("font-weight", "bold")
            .attr("text-anchor", "start")
            .text("Buffer Size");

    g.append("g")
        .attr("class", "axis")
        .call(d3.axisLeft(y).ticks(null, "s"))
        // .call(d3.axisLeft(y).ticks(20))
        .append("text")
            .attr("x", 2)
            .attr("y", y(y.ticks().pop()) + 0.5)
            .attr("dy", "0.32em")
            .attr("fill", "#000")
            .attr("font-weight", "bold")
            .attr("text-anchor", "start")
            .text("QoE");

    // var dataL = 0;
    // var offset = 80;

    var legend = g.append("g")
        .attr("font-family", "sans-serif")
        .attr("font-size", 10)
        .attr("text-anchor", "end")
        .selectAll("g")
        // .data(keys.slice().reverse())
        .data(keys)
        .enter().append("g")
        .attr("transform", function(d, i) { 
            // if (i === 0) {
            //     dataL = d.length + offset 
            //     return "translate(0,0)"
            // } else { 
            // var newdataL = dataL
            // dataL +=  d.length + offset
            // return "translate(" + (-newdataL) + ",-10)"
            // }
            return "translate(80," + i * 20 + ")";
        });

    legend.append("rect")
        .attr("x", width - 19)
        .attr("width", 19)
        .attr("height", 19)
        .attr("fill", function(d,i) {return z(i)});

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9.5)
        .attr("dy", "0.32em")
        .text(function(d) { return d; });
}