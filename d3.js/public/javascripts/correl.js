/** Compute Pearson's correlation coefficient */
export default function computePearsons(arrX, arrY) {
    var num = covar(arrX, arrY);
    var denom = d3.deviation(arrX) * d3.deviation(arrY);
    return num / denom;
}
/** Computes the covariance between random variable observations
 * arrX and arrY
 */
var covar = function (arrX, arrY) {
    var u = d3.mean(arrX);
    var v = d3.mean(arrY);
    var arrXLen = arrX.length;
    var sq_dev = new Array(arrXLen);
    var i;
    for (i = 0; i < arrXLen; i++)
        sq_dev[i] = (arrX[i] - u) * (arrY[i] - v);
    return d3.sum(sq_dev) / (arrXLen - 1);
}