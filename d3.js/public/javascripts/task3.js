import dynamicSort from './dynamicSort.js'
import calcLinear from './calcLinear.js'

export function task_3(data, buf) {
    buf = buf || 30;
    console.log('buf', buf)
    var bufindex;

    switch(buf) {
        case 30: 
        case 60: 
            bufindex = 0;
            break;
        case 120:
            bufindex = 1;
            break;
        case 240:
            bufindex = 2;
            break;
        default:
            bufindex = 0;
    }

    console.log('bufindex', bufindex)

    // d3.select('body').append('div').attr('class', 'equation')
    // d3.select('body').append('div').attr('class', 'equation')
    d3.select('body').append('div').attr('id', 'buffersize').text('Buffer Capacity = '+buf+' Hz');
    d3.select('body').append('div').attr('id', 'correl')

    // var {svg, margin, width, height,g} = par;
    // console.log(svg, width, height, g)
    var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 450 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

    var x = d3.scaleLinear()
        .range([0, width]);

    var y = d3.scaleLinear()
        .range([height, 0]);

    var color = d3.scaleOrdinal(d3.schemeCategory10)

    var xAxis = d3.axisBottom(x);

    var yAxis = d3.axisLeft(y);

    d3.select('svg').remove();
    
    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // svg.width += width + margin.left + margin.right
    // svg.height += height + margin.top + margin.bottom

    // d3.tsv("data/iris.tsv", function(error, data) {
    //     if (error) throw error;

    //     data.forEach(function(d) {
    //         d.sepalLength = +d.sepalLength;
    //         d.sepalWidth = +d.sepalWidth;
    //     });


    var meth_qual_ineff_buf = data.map( d => {
        return {
            quality: d.quality,
            inefficiency: d.inefficiency,
            method: d.method,
            bufSize: d.bufSize
        }
    })
    
    console.log(meth_qual_ineff_buf)
    
    var grouped_data_quality = d3.nest()
        .key(function(d) { return d.bufSize;})
        .key(function(d) { return d.method;})
        .rollup(function(d) { 
            return d3.mean(d, function(g) {return g.quality; });
        })
        .entries(meth_qual_ineff_buf)
        .map( group => {
            var obj = {}
            obj.bufSize = group.key
            var key_ = group.values.map(g => g.key)
            // console.log(key_)
            var val_ = group.values.map(g => g.value)
            // console.log(val_)
            for (var i=0; i < key_.length; ++i)
                obj[key_[i]] = val_[i]
            // console.log(obj)
            return obj;
        })

    // console.log(grouped_data_quality)
    grouped_data_quality.sort(dynamicSort('bufSize'))
    grouped_data_quality.push(grouped_data_quality[2])
    grouped_data_quality[2] = grouped_data_quality[1]
    grouped_data_quality[1] = grouped_data_quality[0]
    grouped_data_quality[0] = grouped_data_quality[3]
    grouped_data_quality.pop()
    console.log(grouped_data_quality)

    // grouped_data_ineff
    var grouped_data_ineff = d3.nest()
        .key(function(d) { return d.bufSize;})
        .key(function(d) { return d.method;})
        .rollup(function(d) { 
            return d3.mean(d, function(g) {return g.inefficiency; });
        })
        .entries(meth_qual_ineff_buf)
        .map( group => {
            var obj = {}
            obj.bufSize = group.key
            var key_ = group.values.map(g => g.key)
            // console.log(key_)
            var val_ = group.values.map(g => g.value)
            // console.log(val_)
            for (var i=0; i < key_.length; ++i)
                obj[key_[i]] = val_[i]
            // console.log(obj)
            return obj;
        })

    // console.log(grouped_data_ineff)
    grouped_data_ineff.sort(dynamicSort('bufSize'))
    grouped_data_ineff.push(grouped_data_ineff[2])
    grouped_data_ineff[2] = grouped_data_ineff[1]
    grouped_data_ineff[1] = grouped_data_ineff[0]
    grouped_data_ineff[0] = grouped_data_ineff[3]
    grouped_data_ineff.pop()
    console.log(grouped_data_ineff)

    var keys = Object.keys(grouped_data_quality[0]).slice(1)
    console.log(keys)

    grouped_data_quality.map(d => delete d.bufSize)
    grouped_data_ineff.map(d => delete d.bufSize)
    
    var multivar = []
    for(var k=0; k<3; k++) {
        multivar[k] = []
        var ineff = []
        Object.values(grouped_data_ineff[k]).map(d=> ineff.push(d))
        Object.values(grouped_data_quality[k]).map( (d,i) => multivar[k].push({quality: d, inefficiency: ineff[i]}))
        
        console.log('multivar['+k+']', multivar[k])
    }
    
    var multivar_data = multivar[bufindex].slice() // filter for buffer size

    // x.domain(d3.extent(multivar_data, function(d) { return d.inefficiency; })).nice();
    y.domain(d3.extent(multivar_data, function(d) { return d.quality; })).nice();

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text("Inefficiency");

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("Quality")

    svg.selectAll(".dot")
        .data(multivar_data)
        .enter().append("circle")
        .attr("class", "dot")
        .attr("r", 3.5)
        .attr("cx", function(d) { return x(d.inefficiency); })
        .attr("cy", function(d) { return y(d.quality); })
        .style("fill", function(d,i) { return color(i); });

    var legend = svg.selectAll(".legend")
        .data(color.domain())
        .enter().append("g")
        .attr("class", "legend")
        .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

    legend.append("rect")
        .attr("x", width - 18)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

    legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function(d,i) { return 'Method '+(i+1); });

    // see below for an explanation of the calcLinear function
    var lg = calcLinear(multivar_data, "x", "y", d3.min(multivar_data, function(d){ return d.inefficiency}), d3.min(multivar_data, function(d){ return d.quality}));

    svg.append("line")
        .attr("class", "regression")
        .attr("x1", x(lg.ptA.x))
        .attr("y1", y(lg.ptA.y))
        .attr("x2", x(lg.ptB.x))
        .attr("y2", y(lg.ptB.y));

    
}
