var data = [1,2,3]

d3.select('body').append('svg')
    .attr('width', 400)
    .attr('height', 300)

d3.select('svg').append('rect').attr('width', 200).attr('height', 160).attr('fill', 'black').attr('id', 'box')

d3.select('svg')
    .append('g')
        .attr('id', 'childbox')

d3.select('#childbox')
    .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
            .attr('width', 20)
            .attr('height', 20)
            .attr('fill', 'white')
            .attr('x', function(d) {return d*30})
            .attr('y', 20)
