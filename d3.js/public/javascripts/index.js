import {task_1a, task_1b} from './task1.js'
import {task_2} from './task2.js'
import {task_3} from './task3.js'
import {task_4} from './task4.js'

var svg = d3.select("svg"),
    margin = {top: 20, right: 80, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom,
    g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var x0 = d3.scaleBand()
    .rangeRound([0, width])
    .paddingInner(0.1);

var x1 = d3.scaleBand()
    .padding(0.05);

var y = d3.scaleLinear()
    .rangeRound([height, 0]);

// var z = d3.scaleOrdinal(d3.schemeCategory20c)
// var z = d3.schemeReds[9]
// var z = d3.scaleOrdinal()
    // .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00", "#85c2ba", "#baebae"]);
// var z = d3.scaleLinear(([10,80], ['green']))
// var z = d3.scaleSequential().domain([0, 10]).interpolator(d3.interpolateGreens);
// var z = d3.scaleSequential().domain([1,10]).interpolator(d3.interpolatePuRd);
// var z = d3.scaleLinear()
    // .domain([0, 1])
    // .range(["red","blue"]);
var z = d3.scaleLinear().domain([1,10]).range(['#D3DBFF', '#002EFF'])

var par = {svg, margin, width, height, g, x0, x1, y, z}

d3.csv("data/results.csv", (d, i, columns) => {
    for (var i = 3, n = columns.length; i < n-1; ++i) 
        d[columns[i]] = +d[columns[i]];
    return d;
}, (err, data) => {

    if (err) throw error;
    par.data = data;
    task_1a(par);
    // task_1b(par);
    // task_2(par);
    // task_3(data, 30);   // provide buffer size in numeric 30/60/120/240
    // task_3(data, 120);   // provide buffer size in numeric 30/60/120/240
    // task_3(data, 240);   // provide buffer size in numeric 30/60/120/240
    // task_4(par);
})